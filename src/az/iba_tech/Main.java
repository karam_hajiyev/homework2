package az.iba_tech;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Random rand = new Random();
        Scanner sc = new Scanner(System.in);

        char[][] arr = new char[5][5];

        for (int i = 0; i < arr.length; i++) {
            int row = rand.nextInt(4);
            int col = rand.nextInt(4);
            arr[row][col] = '-';
        }
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arr[i][j] != '-')
                    arr[i][j] = ' ';
            }
        }
        print(arr);
        while (true) {
            System.out.println("Please Enter the row (1-5)");
            int rowInput = sc.nextInt();
            System.out.println("Please Enter the column (1-5)");
            int colInput = sc.nextInt();
            if (arr[rowInput - 1][colInput - 1] == '-') {
                arr[rowInput - 1][colInput - 1] = 'x';
                print(arr);
            } else {
                arr[rowInput - 1][colInput - 1] = '*';
                print(arr);
            }
            for (int i = 0; i < arr.length; i++) {
                for (int j = 0; j < arr.length; j++) {
                    if (arr[i][j] != '-')
                        break;
                }
            }
        }
    }

    public static void print(char arr[][]) {
        System.out.println("| " + arr[0][0] + " | "
                + arr[0][1] + " | " + arr[0][2] + " | " +
                arr[0][3] + " | " + arr[0][4] + " |");
        System.out.println();
        System.out.println("| " + arr[1][0] + " | "
                + arr[1][1] + " | " + arr[1][2] + " | " +
                arr[1][3] + " | " + arr[1][4] + " |");
        System.out.println();
        System.out.println("| " + arr[2][0] + " | "
                + arr[2][1] + " | " + arr[2][2] + " | " +
                arr[2][3] + " | " + arr[2][4] + " |");
        System.out.println();
        System.out.println("| " + arr[3][0] + " | "
                + arr[3][1] + " | " + arr[3][2] + " | " +
                arr[3][3] + " | " + arr[3][4] + " |");
        System.out.println();
        System.out.println("| " + arr[4][0] + " | "
                + arr[4][1] + " | " + arr[4][2] + " | " +
                arr[4][3] + " | " + arr[4][4] + " |");
    }
}

